-- SUMMARY --

This module allows you to see what other customers are viewing as products.
This is a familiar feature on commerce website.

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2070709


-- REQUIREMENTS --

* Drupal Commerce

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Go to 'admin/structure/block',
  place block "View: What people are viewing: Products people are viewing now"
  where you want.

-- CONFIGURATION --

* You can configure the view at admin/structure/views/view/commerce_cavn

-- CONTACT --

Current maintainers:
* Yohan TILLIER (garnett2125) - http://drupal.org/user/1281666
